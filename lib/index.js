"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _compromise = _interopRequireDefault(require("compromise"));

var _lda = _interopRequireDefault(require("lda"));

var _synonyms = _interopRequireDefault(require("synonyms"));

var _restify = _interopRequireDefault(require("restify"));

var _associations = _interopRequireDefault(require("./associations.json"));

var _axios = _interopRequireDefault(require("axios"));

var _restifyCorsMiddleware = _interopRequireDefault(require("restify-cors-middleware"));

var _express = require("express");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

_compromise["default"].extend(require("compromise-adjectives"));

_compromise["default"].extend(require("compromise-numbers"));

var REG_ATTCEPT_ONLY_LETTERS = /^[a-zA-Z]+/g;
var PARA_MIN_NUMBER_OF_LETTERS = 40;
var MIN_NUMBER_OF_WORDS = 120;
var MAX_NUMBER_OF_WORDS = 150;
var THEME_LIMIT = 50;
var PARA_THEME_LIMIT = 55;
var MOST_REPEATED_WORDS = 3;

var TextStater = /*#__PURE__*/function () {
  function TextStater(text) {
    _classCallCheck(this, TextStater);

    this.text = text.replace(/\n\n/g, " ");
    this.nlp = (0, _compromise["default"])(text);
    this.rawText = text;
  }

  _createClass(TextStater, [{
    key: "_getWords",
    value: function _getWords() {
      return this.nlp.termList().map(function (term) {
        return term.text;
      }).filter(function (value) {
        return value != "";
      });
    }
  }, {
    key: "_getWordsFiltered",
    value: function _getWordsFiltered() {
      return this.nlp.termList().map(function (term) {
        return term.text;
      }).filter(function (value, index, array) {
        return array.indexOf(value) == index && value != "";
      });
    }
  }, {
    key: "getNumberOfWords",
    value: function getNumberOfWords() {
      return this._getWordsFiltered().length;
    }
  }, {
    key: "_getSentences",
    value: function _getSentences() {
      return this.nlp.sentences().map(function (sentence) {
        return sentence.text();
      });
    }
  }, {
    key: "_getTheme",
    value: function _getTheme() {
      return (0, _lda["default"])(this._getSentences(), 1, 10)[0].map(function (word) {
        return word.term;
      });
    }
  }, {
    key: "_getNouns",
    value: function _getNouns() {
      return this.nlp.nouns().toSingular().map(function (noun) {
        return noun.text().match(REG_ATTCEPT_ONLY_LETTERS);
      }).map(function (text) {
        return text[0].toLowerCase();
      });
    }
  }, {
    key: "_getVerbs",
    value: function _getVerbs() {
      return this.nlp.verbs().map(function (verb) {
        return verb.text().match(REG_ATTCEPT_ONLY_LETTERS);
      }).map(function (text) {
        return text[0].toLowerCase();
      });
    }
  }, {
    key: "_getAdjectives",
    value: function _getAdjectives() {
      return this.nlp.adjectives().map(function (verb) {
        return verb.text().match(REG_ATTCEPT_ONLY_LETTERS);
      }).map(function (text) {
        return text[0].toLowerCase();
      });
    }
  }, {
    key: "_getSynonyms",
    value: function _getSynonyms(words) {
      var preproWords = [];
      words.forEach(function (word) {
        var syno = (0, _synonyms["default"])(word) || {};
        if (syno.v) preproWords.push.apply(preproWords, _toConsumableArray(syno.v));
        if (syno.n) preproWords.push.apply(preproWords, _toConsumableArray(syno.n));
      });
      return preproWords.filter(function (value, index, array) {
        return array.indexOf(value) == index && value != "";
      });
    }
  }, {
    key: "_getAdverbs",
    value: function _getAdverbs() {
      return this.nlp.adverbs().map(function (verb) {
        return verb.text().match(REG_ATTCEPT_ONLY_LETTERS);
      }).map(function (text) {
        return text[0].toLowerCase();
      });
    }
  }, {
    key: "topicEvaluating",
    value: function topicEvaluating(keywords, title) {
      title = title || "";
      keywords = keywords || [];

      var synonyms = _toConsumableArray(this._getWords());

      var keys = _toConsumableArray(keywords);

      if (this._getTheme().length) synonyms.push.apply(synonyms, _toConsumableArray(this._getTheme()));
      if (this._getNouns().length) synonyms.push.apply(synonyms, _toConsumableArray(this._getNouns()));
      if (this._getVerbs().length) synonyms.push.apply(synonyms, _toConsumableArray(this._getVerbs()));
      if (this._getAdverbs().length) synonyms.push.apply(synonyms, _toConsumableArray(this._getAdverbs()));

      if (title !== "") {
        var tit = new TextStater(title);
        var prepro = [];
        if (tit._getTheme().length) prepro.push.apply(prepro, _toConsumableArray(tit._getTheme()));
        if (tit._getNouns().length) prepro.push.apply(prepro, _toConsumableArray(tit._getNouns()));
        if (tit._getVerbs().length) prepro.push.apply(prepro, _toConsumableArray(tit._getVerbs()));
        if (tit._getAdverbs().length) prepro.push.apply(prepro, _toConsumableArray(tit._getAdverbs()));
        var tit_synonyms = prepro;

        if (prepro.length > 0) {
          tit_synonyms.push.apply(tit_synonyms, _toConsumableArray(tit._getSynonyms(tit._getSynonyms(prepro))));
        }

        keys.push.apply(keys, tit_synonyms);
      }

      keys.forEach(function (key) {
        if (_associations["default"][key]) keys.push.apply(keys, _toConsumableArray(_associations["default"][key]));
      });
      return keys.filter(function (value) {
        return synonyms.includes(value);
      }).filter(function (value, index, array) {
        return array.indexOf(value) == index && value != "";
      });
    }
  }, {
    key: "_getExpandedText",
    value: function _getExpandedText() {
      var sent = this.nlp.contractions().map(function (sent) {
        return sent.text();
      });
      var conSent = this.nlp.contractions().expand().map(function (sent) {
        return sent.text();
      });
      var text = this.text;

      for (var i in sent) {
        text = text.replace(sent[i], conSent[i]);
      }

      return text;
    }
  }, {
    key: "_getHybernated",
    value: function _getHybernated() {
      return this.nlp.hyphenated().map(function (hyb) {
        return hyb.text();
      });
    }
  }, {
    key: "_getClauses",
    value: function _getClauses() {
      return this.nlp.clauses().map(function (cl) {
        return cl.text();
      });
    }
  }, {
    key: "_getSentenceTimes",
    value: function _getSentenceTimes() {
      var text = new TextStater(this._getExpandedText());

      var sent = text._getSentences().map(function (sen) {
        return new TextStater(sen)._getVerbs();
      });

      var fin = sent.map(function (sen) {
        return sen.map(function (verb) {
          var times = (0, _compromise["default"])(verb).verbs().conjugate();

          if (times.length && times.length > 0) {
            var val = Object.values(times[0]);
            var keys = Object.keys(times[0]);

            for (var i in val) {
              if (verb == val[i]) {
                return keys[val.indexOf(val[i])] ? keys[val.indexOf(val[i])] : "Infinitive";
              }
            }
          }
        });
      });
      return fin.map(function (sn) {
        return sn.filter(function (sen) {
          return sen !== undefined;
        });
      });
    }
  }, {
    key: "getTextTimes",
    value: function getTextTimes() {
      var res = [];

      this._getSentenceTimes().forEach(function (time) {
        if (time.length) res.push.apply(res, _toConsumableArray(time));
      });

      return res;
    }
  }, {
    key: "_getNamesInText",
    value: function _getNamesInText(text) {
      return (0, _compromise["default"])(text).people().map(function (person) {
        return person.text();
      });
    }
  }, {
    key: "_getAbbreviationsInText",
    value: function _getAbbreviationsInText(text) {
      return (0, _compromise["default"])(text).abbreviations().map(function (abb) {
        return abb.text();
      });
    }
  }, {
    key: "_getParagraphs",
    value: function _getParagraphs() {
      return this.rawText.split(/\n\n/g);
    }
  }, {
    key: "_getNumberOfParagraphs",
    value: function _getNumberOfParagraphs() {
      return this._getParagraphs().length;
    }
  }, {
    key: "getNumberOfWordsInParagraphs",
    value: function getNumberOfWordsInParagraphs() {
      return this._getParagraphs().map(function (para) {
        return new TextStater(para)._getWords().length;
      });
    }
  }, {
    key: "_getConjunctions",
    value: function _getConjunctions() {
      return this.nlp.conjunctions().map(function (con) {
        return con.text();
      });
    }
  }, {
    key: "getConjunctionsRepeating",
    value: function getConjunctionsRepeating() {
      var arr = this._getConjunctions().sort();

      var a = [],
          b = [],
          prev;
      arr.sort();

      for (var i = 0; i < arr.length; i++) {
        if (arr[i] !== prev) {
          a.push(arr[i]);
          b.push(1);
        } else {
          b[b.length - 1]++;
        }

        prev = arr[i];
      }

      return [a, b];
    }
  }, {
    key: "_getParagraphTopic",
    value: function _getParagraphTopic(para, title, keywords) {
      var text = new TextStater(para);
      title = title || "";
      keywords = keywords || [];

      var synonyms = _toConsumableArray(text._getWords());

      var keys = _toConsumableArray(keywords);

      if (text._getTheme().length) synonyms.push.apply(synonyms, _toConsumableArray(text._getTheme()));
      if (text._getNouns().length) synonyms.push.apply(synonyms, _toConsumableArray(text._getNouns()));
      if (text._getVerbs().length) synonyms.push.apply(synonyms, _toConsumableArray(text._getVerbs()));
      if (text._getAdverbs().length) synonyms.push.apply(synonyms, _toConsumableArray(text._getAdverbs()));

      if (title !== "") {
        var tit = new TextStater(title);
        var prepro = [];
        if (tit._getTheme().length) prepro.push.apply(prepro, _toConsumableArray(tit._getTheme()));
        if (tit._getNouns().length) prepro.push.apply(prepro, _toConsumableArray(tit._getNouns()));
        if (tit._getVerbs().length) prepro.push.apply(prepro, _toConsumableArray(tit._getVerbs()));
        if (tit._getAdverbs().length) prepro.push.apply(prepro, _toConsumableArray(tit._getAdverbs()));
        var tit_synonyms = prepro;

        if (prepro.length > 0) {
          tit_synonyms.push.apply(tit_synonyms, _toConsumableArray(tit._getSynonyms(tit._getSynonyms(prepro))));
        }

        keys.push.apply(keys, tit_synonyms);
      }

      keys.forEach(function (key) {
        if (_associations["default"][key]) keys.push.apply(keys, _toConsumableArray(_associations["default"][key]));
      });
      return keys.filter(function (value) {
        return synonyms.includes(value);
      }).filter(function (value, index, array) {
        return array.indexOf(value) == index && value != "";
      });
    }
  }, {
    key: "paragraphTopicEvaluating",
    value: function paragraphTopicEvaluating(keywords, topics) {
      keywords = keywords || [[]];
      topics = topics || [[]];
      var synonyms = [];

      var para = this._getParagraphs().filter(function (para) {
        return para.length > PARA_MIN_NUMBER_OF_LETTERS;
      });

      for (var i in para) {
        synonyms.push(this._getParagraphTopic(para[i], topics[i], keywords[i]));
      }

      return synonyms;
    }
  }, {
    key: "paragraphDifferenceCalculating",
    value: function paragraphDifferenceCalculating() {
      return this._getParagraphs().filter(function (para) {
        return para.length > PARA_MIN_NUMBER_OF_LETTERS;
      }).map(function (para) {
        return new TextStater(para).getNumberOfWords();
      });
    }
  }, {
    key: "findRepeatingWords",
    value: function findRepeatingWords() {
      var arr = [].concat(_toConsumableArray(this._getAdverbs()), _toConsumableArray(this._getNouns()), _toConsumableArray(this._getVerbs()), _toConsumableArray(this._getAdjectives())).sort();
      var a = [],
          b = [],
          prev;
      arr.sort();

      for (var i = 0; i < arr.length; i++) {
        if (arr[i] !== prev) {
          a.push(arr[i]);
          b.push(1);
        } else {
          b[b.length - 1]++;
        }

        prev = arr[i];
      }

      return [a, b];
    }
  }]);

  return TextStater;
}();

var _default = TextStater;
exports["default"] = _default;

var GraduationEvaluator = /*#__PURE__*/function () {
  function GraduationEvaluator(topic, keys, paragraphTopics, paragraphKeys, text) {
    _classCallCheck(this, GraduationEvaluator);

    this.topic = topic;
    this.keys = keys;
    this.paragraphTopics = paragraphTopics;
    this.paragraphKeys = paragraphKeys;
    this.text = text;
    this.stater = new TextStater(text);
  }

  _createClass(GraduationEvaluator, [{
    key: "__test",
    value: function __test() {
      console.log("\n----------------------------------------------");
      console.log("Téma: ", this.topic);
      console.log("Klíčová slova: ", this.keys);
      console.log("Klíčové body odstavců: ", this.paragraphTopics);
      console.log("Klíčová slova odstavců: ", this.paragraphKeys);
      console.log("----------------------------------------------");
      console.log("Text:\n", this.text);
      console.log("----------------------------------------------");
    }
  }, {
    key: "__calcTest",
    value: function __calcTest() {
      console.log("----------------------------------------------");
      console.log("Téma splněno na: ", this.content(), "%");
      console.log("Počet slov: ", this.textLength());
      console.log("Odstavce dodrženy na: ", this.contentRange(), "%");
      console.log("Obsah odstavců dodržen na: ", this.contentDetails());
      console.log("Koheze textu: ", this.cohesion(), "%");
      console.log("Organizace textu: ", this.organisation());
      console.log("Struktura dodržena: ", this.structure());
      console.log("Počet často opakujících se slov: ", this.textRepetition());
      console.log("Unikátnost slov: ", this.vocabulary(), "%");
      console.log(this.textContinuityMeansRange());
    }
  }, {
    key: "content",
    value: function content() {
      var res = this.stater.topicEvaluating(this.keys, this.topic).length * 100 / this.stater.getNumberOfWords() * 100 / THEME_LIMIT;
      return res > 100 ? 100 : res;
    }
  }, {
    key: "textLength",
    value: function textLength() {
      return this.stater.getNumberOfWords();
    }
  }, {
    key: "contentRange",
    value: function contentRange() {
      var para = this.stater._getParagraphs().filter(function (para) {
        return para.length > PARA_MIN_NUMBER_OF_LETTERS;
      }).length;

      var points = this.paragraphTopics.length;

      if (points > para) {
        return para * 100 / points;
      } else if (para > points) {
        return points * 100 / para;
      } else return 100;
    }
  }, {
    key: "contentDetails",
    value: function contentDetails() {
      var paraNum = this.stater.getNumberOfWordsInParagraphs().filter(function (para) {
        return para > 8;
      });
      var paraTopics = this.stater.paragraphTopicEvaluating(this.paragraphKeys, this.paragraphTopics);
      var topicResult = paraTopics.map(function (topic) {
        return topic.length * 100 / paraNum[paraTopics.indexOf(topic)];
      });
      return topicResult.map(function (res) {
        return res * 100 / PARA_THEME_LIMIT > 100 ? 100 : res * 100 / PARA_THEME_LIMIT;
      });
    }
  }, {
    key: "textContinuityMeansRange",
    value: function textContinuityMeansRange() {
      var cons = this.stater.getConjunctionsRepeating();
      var len = cons[0].length;
      var sum = cons[1].reduce(function (p, c) {
        return p + c;
      }, 0);
      var res = Math.ceil(sum / 5) - len;
      return res <= 0 ? 0 : res;
    }
  }, {
    key: "organisation",
    value: function organisation() {
      var para = this.stater._getParagraphs();

      var firstName = this.stater._getNamesInText(para[0]).length;

      var first = this.stater._getAbbreviationsInText(para[0]).length;

      var last = this.stater._getNamesInText(para[para.length - 1]).length;

      return [(first > 0 || firstName > 0) && para[0].length < 40, last > 0 && para[para.length - 1].length < 40];
    }
  }, {
    key: "cohesion",
    value: function cohesion() {
      var times = this.stater.getTextTimes();
      var last = times[0];
      var count = 0;
      times.forEach(function (time) {
        if (time != last) {
          ++count;
          last = time;
        }
      });
      return count * 100 / this.stater._getSentences().length;
    }
  }, {
    key: "structure",
    value: function structure() {
      var num = this.stater.paragraphDifferenceCalculating();
      var dif = (MAX_NUMBER_OF_WORDS - 10) / num.length;
      var coef = dif / 2;
      var res = num.map(function (met) {
        return dif - coef < met && dif + coef > met;
      });
      return res;
    }
  }, {
    key: "textRepetition",
    value: function textRepetition() {
      var rep = this.stater.findRepeatingWords();
      var filt = [];

      for (var i in rep[1]) {
        if (rep[1][i] > MOST_REPEATED_WORDS) {
          for (var j = 0; j < Math.floor(rep[1][i] / MOST_REPEATED_WORDS); j++) {
            filt.push(rep[1][i]);
          }
        }
      }

      console.log(filt);
      return filt.length;
    }
  }, {
    key: "vocabulary",
    value: function vocabulary() {
      var rep = this.stater.findRepeatingWords()[1];
      var avg = rep.reduce(function (p, c) {
        return p + c;
      }, 0) / rep.length;
      return 100 / avg > 0 ? 100 / avg : 0;
    }
  }, {
    key: "grammarMeansRange",
    value: function grammarMeansRange() {
      var coef = [];
      if (this.stater._getHybernated().length && this.stater._getHybernated().length > 0) coef.push.apply(coef, _toConsumableArray(this.stater._getHybernated()));
      if (this.stater._getClauses().length && this.stater._getClauses().length > 0) coef.push.apply(coef, _toConsumableArray(this.stater._getClauses()));
      if (this.stater._getAdverbs().length && this.stater._getAdverbs().length > 0) coef.push.apply(coef, _toConsumableArray(this.stater._getAdverbs()));
      if (this.stater._getConjunctions().length && this.stater._getConjunctions().length > 0) coef.push.apply(coef, _toConsumableArray(this.stater._getConjunctions()));
      if (this.stater._getTheme().length && this.stater._getTheme().length > 0) coef.push.apply(coef, _toConsumableArray(this.stater._getTheme()));
      return coef.map(function (co) {
        return co[co.length - 1] == " " ? co.slice(0, co.length - 1).toLowerCase() : co.toLowerCase();
      });
    }
  }]);

  return GraduationEvaluator;
}(); // const topic =
//   "Unformal letter for friend Lucy about reccently bought house in USA";
// const topicKeys = ["letter", "move", "house", "visit", "new", "past"];

/*
const topic = "Two nasa pilots discovered new planet";
const topicKeys = ["space", "nasa", "pilots", "planets", "ship"];
*/
// const paragraphTopics = [
//   "Tell about reason, why are you writing a letter.",
//   "Describe new house a lot and compare him with your recent house.",
//   "Tell about good thinks and possitives of new house, also mention negatives and bad things and describe why.",
//   "Invite Lucy for visit your house."
// ];
// const paragraphKeys = [
//   ["reason", "writing", "letter", "why", "explain"],
//   ["describe", "new", "house", "past", "compare"],
//   ["possitives", "negatives", "house", "describe", "why"],
//   ["invite", "invitation", "visit", "home", "future"]
// ];
// const text =
//   "Hi Lucy,\n\nI‘m writing to you because I have just moved to a new flat in the city. I‘d like to tell you some news about it because it‘s just great in here.\n\nOur new flat‘s very large and fully furnished. It’s also equipped with appliances. There are five spacious rooms, a modern kitchen and a balcony with a beautiful view of the city. The place is much larger than the flat we lived in before. You’d love it.\n\nI like my new neighbours. When we arrived, they came right away and introduced themselves. There‘s only one thing I don’t like. It‘s the distance from here to my school because I have to wake up early.\n\nI think you must come to my new place and see all of it. Do you have time this Friday?\n\n See you,\n\n Caroline";

/*
const text =
  "Hello Mary,\n\nHow are you? I am super. I am writing because I want to describe you my house. there live with a family and is very big, a beautiful, a pleasing. At home is ten rooms which have small, big, yellow, white, red, green and blue. Our house have green colour, white windows, brown door and red a roof.\n\n Our new house is about a lot beautiful than our old house. At new house is life nice beacuse is tidy and there are all news and also have beautiful garden and a swimming pool. And red roof and brown door and shote windows are nice also.\n\n I am invite you also to my home because I describe you my house.\n\n See you soon,\n\nTom";

*/
// const eva = new GraduationEvaluator(
//   topic,
//   topicKeys,
//   paragraphTopics,
//   paragraphKeys,
//   text
// );
// eva.__test();
// eva.__calcTest();


var ResultCalculator = /*#__PURE__*/function () {
  function ResultCalculator(evalu) {
    _classCallCheck(this, ResultCalculator);

    this.evalu = evalu;
  }

  _createClass(ResultCalculator, [{
    key: "getResult",
    value: function getResult(cb) {
      var _this = this;

      _axios["default"].post("http://127.0.0.1:8004/api", {
        text: this.evalu.text
      }).then(function (res) {
        cb({
          id: ["I – Zpracování", "II – Organizace", "III – Slovní zásoba", "IV – Mluvnické prostředky"],
          A: [_this.part1a(), _this.part2a(), _this.part3a(res.data), _this.part4a(res.data)],
          B: [_this.part1b(), _this.part2b(), _this.part3b(), _this.part4b(res.data)]
        });
      });
    }
  }, {
    key: "countPartResult",
    value: function countPartResult(arr) {
      var avg = arr.reduce(function (p, c) {
        return p + c;
      }, 0) / arr.length;
      if (arr[0] > avg) return Math.ceil(avg);else return Math.floor(avg);
    }
  }, {
    key: "countMarkFromPercentage",
    value: function countMarkFromPercentage(num) {
      if (num < 48) return 0;else if (num < 64) return 1;else if (num < 80) return 2;
      return 3;
    }
  }, {
    key: "countMarkFromDownPercentage",
    value: function countMarkFromDownPercentage(num) {
      if (num > 59) return 0;else if (num > 46) return 1;else if (num > 33) return 2;
      return 3;
    }
  }, {
    key: "countMarkFromNumberOfItems",
    value: function countMarkFromNumberOfItems(num) {
      if (num <= 3) return 3;else if (num <= 6) return 2;else if (num <= 9) return 1;else return 0;
    }
  }, {
    key: "countMarkFromMiddlePercentage",
    value: function countMarkFromMiddlePercentage(num) {
      if (num > 68) return 3;else if (num > 56) return 2;else if (num > 44) return 1;
      return 0;
    }
  }, {
    key: "part1a",
    value: function part1a() {
      var resultA = 3;
      var resultB = 3;
      var resultC = 3;

      for (var i in this.evalu.organisation()) {
        if (!this.evalu.organisation()[i]) resultB--;
      }

      for (var _i in this.evalu.structure()) {
        if (!this.evalu.structure()[_i]) resultA--;
      }

      var wordCount = this.evalu.textLength;
      if (wordCount > 150 && wordCount <= 180 || wordCount < 120 && wordCount >= 90) resultC - 1;else if (wordCount > 180 && wordCount <= 210 || wordCount < 90 && wordCount >= 60) resultC - 2;else if (wordCount > 210 || wordCount < 60) resultC - 3;
      return resultA == 0 ? 0 : this.countPartResult([resultA, resultB == 1 ? 0 : resultB, resultC]);
    }
  }, {
    key: "part1b",
    value: function part1b() {
      var _this2 = this;

      var resultA = 3;
      var resultB = 3;
      var resultC = 3;

      for (var i in this.evalu.structure()) {
        if (!this.evalu.structure()[i]) resultA--;
      }

      resultB = this.countMarkFromPercentage(this.evalu.content());
      resultA = this.countPartResult(this.evalu.contentDetails().map(function (per) {
        return _this2.countMarkFromPercentage(per);
      }));
      resultC = this.countMarkFromPercentage(this.evalu.content());
      return resultA == 0 ? 0 : this.countPartResult([resultA, resultB, resultC]);
    }
  }, {
    key: "part2a",
    value: function part2a() {
      var _this3 = this;

      var resultA = 3;
      var resultB = 3;
      resultA = this.countMarkFromDownPercentage(this.evalu.cohesion());
      resultB = this.countPartResult(this.evalu.contentDetails().map(function (per) {
        return _this3.countMarkFromPercentage(per);
      }));
      return this.countPartResult([resultA, resultB]);
    }
  }, {
    key: "part2b",
    value: function part2b() {
      var resultB = 3;
      var resultC = 3;
      resultC = this.countMarkFromNumberOfItems(this.evalu.textRepetition());
      resultB -= this.evalu.textContinuityMeansRange();
      return this.countPartResult([resultB <= 0 ? 0 : resultB, resultC]);
    }
  }, {
    key: "part3a",
    value: function part3a(mistakes) {
      var grammar = [];
      var style = [];
      mistakes.forEach(function (row) {
        if (row.rule == "MORFOLOGIK_RULE_EN_GB") grammar.push(row);else style.push(row);
      });
      var resultA = 3;
      var resultB = 3;
      var resultC = 3;
      resultA -= Math.floor(grammar.length / 3);
      resultB -= Math.floor(style.length / 3);
      var wordCount = this.evalu.textLength;
      if (wordCount < 120 && wordCount >= 90) resultC - 1;else if (wordCount < 90 && wordCount >= 60) resultC - 2;else if (wordCount < 60) resultC - 3;
      var re = [resultA, resultB];
      if (resultC != 3) re.push(resultC);
      return this.countPartResult(re);
    }
  }, {
    key: "part3b",
    value: function part3b() {
      return this.countMarkFromMiddlePercentage(this.evalu.vocabulary());
    }
  }, {
    key: "part4a",
    value: function part4a(mistakes) {
      var gram = mistakes.map(function (row) {
        if (row.rule == "MORFOLOGIK_RULE_EN_GB") return row;else return {};
      }).filter(function (row) {
        return row.position;
      });
      var style = mistakes.map(function (row) {
        if (row.rule != "MORFOLOGIK_RULE_EN_GB") return row;else return {};
      }).filter(function (row) {
        return row.position;
      });
      var resultA = 3;
      var resultB = 3;
      var resultC = 3;
      resultA -= Math.floor(style.length / 5);
      resultB -= Math.floor(gram.length / 3);
      var wordCount = this.evalu.textLength;
      if (wordCount < 120 && wordCount >= 90) resultC - 1;else if (wordCount < 90 && wordCount >= 60) resultC - 2;else if (wordCount < 60) resultC - 3;
      return this.countPartResult([resultA, resultB, resultC]);
    }
  }, {
    key: "part4b",
    value: function part4b(mistakes) {
      var coef = this.evalu.grammarMeansRange();
      var forma = coef.filter(function (value, index, array) {
        return array.indexOf(value) == index && value != "";
      });
      var res = 100 * (forma.length - mistakes.length) / coef.length;
      return this.countMarkFromMiddlePercentage(res);
    }
  }]);

  return ResultCalculator;
}();

var express = require("express");

var bodyParser = require("body-parser");

var cors = require("cors");

var http = require("http");

var config = {
  name: "text-statistics-service",
  port: "8005"
};
var app = express();
var server = http.createServer(app);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors({
  origin: ["*"],
  methods: ["GET", "POST"]
}));
app.get("/", function (req, res, next) {
  res.send("Running ".concat(config.name, " on port: ").concat(config.port, "."));
});
app.post("/api", function (req, res, next) {
  if (req.body.topic && req.body.topicKeys && req.body.paragraphTopics && req.body.paragraphKeys && req.body.text) {
    var calc = new ResultCalculator(new GraduationEvaluator(req.body.topic, req.body.topicKeys, req.body.paragraphTopics, req.body.paragraphKeys, req.body.text));
    calc.getResult(function (result) {
      console.log(result);
      res.send(result);
    });
  } else res.send({
    status: false,
    error: "Missing all data"
  });
});
app.set("port", config.port);
server.listen(config.port);
server.on("listening", function () {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe ".concat(addr) : "port ".concat(addr.port);
  console.log("Listening on ".concat(bind));
});