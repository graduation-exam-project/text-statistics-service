import nlp from "compromise";
nlp.extend(require("compromise-adjectives"));
nlp.extend(require("compromise-numbers"));

import lda from "lda";
import syn from "synonyms";
import restify from "restify";
import assoc from "./associations.json";
import axios from "axios";
import rcm from "restify-cors-middleware";
import { response } from "express";

const REG_ATTCEPT_ONLY_LETTERS = /^[a-zA-Z]+/g;
const PARA_MIN_NUMBER_OF_LETTERS = 40;
const MIN_NUMBER_OF_WORDS = 120;
const MAX_NUMBER_OF_WORDS = 150;
const THEME_LIMIT = 50;
const PARA_THEME_LIMIT = 55;
const MOST_REPEATED_WORDS = 3;

class TextStater {
  constructor(text) {
    this.text = text.replace(/\n\n/g, " ");
    this.nlp = nlp(text);
    this.rawText = text;
  }

  _getWords() {
    return this.nlp
      .termList()
      .map(term => term.text)
      .filter(value => value != "");
  }

  _getWordsFiltered() {
    return this.nlp
      .termList()
      .map(term => term.text)
      .filter((value, index, array) => {
        return array.indexOf(value) == index && value != "";
      });
  }

  getNumberOfWords() {
    return this._getWordsFiltered().length;
  }

  _getSentences() {
    return this.nlp.sentences().map(sentence => sentence.text());
  }

  _getTheme() {
    return lda(this._getSentences(), 1, 10)[0].map(word => word.term);
  }

  _getNouns() {
    return this.nlp
      .nouns()
      .toSingular()
      .map(noun => noun.text().match(REG_ATTCEPT_ONLY_LETTERS))
      .map(text => text[0].toLowerCase());
  }

  _getVerbs() {
    return this.nlp
      .verbs()
      .map(verb => verb.text().match(REG_ATTCEPT_ONLY_LETTERS))
      .map(text => text[0].toLowerCase());
  }

  _getAdjectives() {
    return this.nlp
      .adjectives()
      .map(verb => verb.text().match(REG_ATTCEPT_ONLY_LETTERS))
      .map(text => text[0].toLowerCase());
  }

  _getSynonyms(words) {
    let preproWords = [];
    words.forEach(word => {
      const syno = syn(word) || {};
      if (syno.v) preproWords.push(...syno.v);
      if (syno.n) preproWords.push(...syno.n);
    });

    return preproWords.filter((value, index, array) => {
      return array.indexOf(value) == index && value != "";
    });
  }

  _getAdverbs() {
    return this.nlp
      .adverbs()
      .map(verb => verb.text().match(REG_ATTCEPT_ONLY_LETTERS))
      .map(text => text[0].toLowerCase());
  }

  topicEvaluating(keywords, title) {
    title = title || "";
    keywords = keywords || [];
    let synonyms = [...this._getWords()];
    let keys = [...keywords];

    if (this._getTheme().length) synonyms.push(...this._getTheme());
    if (this._getNouns().length) synonyms.push(...this._getNouns());
    if (this._getVerbs().length) synonyms.push(...this._getVerbs());
    if (this._getAdverbs().length) synonyms.push(...this._getAdverbs());

    if (title !== "") {
      const tit = new TextStater(title);
      let prepro = [];

      if (tit._getTheme().length) prepro.push(...tit._getTheme());
      if (tit._getNouns().length) prepro.push(...tit._getNouns());
      if (tit._getVerbs().length) prepro.push(...tit._getVerbs());
      if (tit._getAdverbs().length) prepro.push(...tit._getAdverbs());

      let tit_synonyms = prepro;

      if (prepro.length > 0) {
        tit_synonyms.push(...tit._getSynonyms(tit._getSynonyms(prepro)));
      }

      keys.push(...tit_synonyms);
    }

    keys.forEach(key => {
      if (assoc[key]) keys.push(...assoc[key]);
    });

    return keys
      .filter(value => {
        return synonyms.includes(value);
      })
      .filter((value, index, array) => {
        return array.indexOf(value) == index && value != "";
      });
  }

  _getExpandedText() {
    const sent = this.nlp.contractions().map(sent => sent.text());

    const conSent = this.nlp
      .contractions()
      .expand()
      .map(sent => sent.text());

    let text = this.text;

    for (let i in sent) {
      text = text.replace(sent[i], conSent[i]);
    }

    return text;
  }

  _getHybernated() {
    return this.nlp.hyphenated().map(hyb => hyb.text());
  }

  _getClauses() {
    return this.nlp.clauses().map(cl => cl.text());
  }

  _getSentenceTimes() {
    const text = new TextStater(this._getExpandedText());

    const sent = text
      ._getSentences()
      .map(sen => new TextStater(sen)._getVerbs());

    const fin = sent.map(sen => {
      return sen.map(verb => {
        const times = nlp(verb)
          .verbs()
          .conjugate();

        if (times.length && times.length > 0) {
          const val = Object.values(times[0]);
          const keys = Object.keys(times[0]);

          for (let i in val) {
            if (verb == val[i]) {
              return keys[val.indexOf(val[i])]
                ? keys[val.indexOf(val[i])]
                : "Infinitive";
            }
          }
        }
      });
    });

    return fin.map(sn => sn.filter(sen => sen !== undefined));
  }

  getTextTimes() {
    let res = [];

    this._getSentenceTimes().forEach(time => {
      if (time.length) res.push(...time);
    });

    return res;
  }

  _getNamesInText(text) {
    return nlp(text)
      .people()
      .map(person => person.text());
  }

  _getAbbreviationsInText(text) {
    return nlp(text)
      .abbreviations()
      .map(abb => abb.text());
  }

  _getParagraphs() {
    return this.rawText.split(/\n\n/g);
  }

  _getNumberOfParagraphs() {
    return this._getParagraphs().length;
  }

  getNumberOfWordsInParagraphs() {
    return this._getParagraphs().map(
      para => new TextStater(para)._getWords().length
    );
  }

  _getConjunctions() {
    return this.nlp.conjunctions().map(con => con.text());
  }

  getConjunctionsRepeating() {
    const arr = this._getConjunctions().sort();

    var a = [],
      b = [],
      prev;

    arr.sort();
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] !== prev) {
        a.push(arr[i]);
        b.push(1);
      } else {
        b[b.length - 1]++;
      }
      prev = arr[i];
    }

    return [a, b];
  }

  _getParagraphTopic(para, title, keywords) {
    const text = new TextStater(para);
    title = title || "";
    keywords = keywords || [];
    let synonyms = [...text._getWords()];
    let keys = [...keywords];

    if (text._getTheme().length) synonyms.push(...text._getTheme());
    if (text._getNouns().length) synonyms.push(...text._getNouns());
    if (text._getVerbs().length) synonyms.push(...text._getVerbs());
    if (text._getAdverbs().length) synonyms.push(...text._getAdverbs());

    if (title !== "") {
      const tit = new TextStater(title);
      let prepro = [];

      if (tit._getTheme().length) prepro.push(...tit._getTheme());
      if (tit._getNouns().length) prepro.push(...tit._getNouns());
      if (tit._getVerbs().length) prepro.push(...tit._getVerbs());
      if (tit._getAdverbs().length) prepro.push(...tit._getAdverbs());

      let tit_synonyms = prepro;

      if (prepro.length > 0) {
        tit_synonyms.push(...tit._getSynonyms(tit._getSynonyms(prepro)));
      }

      keys.push(...tit_synonyms);
    }

    keys.forEach(key => {
      if (assoc[key]) keys.push(...assoc[key]);
    });

    return keys
      .filter(value => {
        return synonyms.includes(value);
      })
      .filter((value, index, array) => {
        return array.indexOf(value) == index && value != "";
      });
  }

  paragraphTopicEvaluating(keywords, topics) {
    keywords = keywords || [[]];
    topics = topics || [[]];
    let synonyms = [];
    const para = this._getParagraphs().filter(
      para => para.length > PARA_MIN_NUMBER_OF_LETTERS
    );

    for (let i in para) {
      synonyms.push(this._getParagraphTopic(para[i], topics[i], keywords[i]));
    }

    return synonyms;
  }

  paragraphDifferenceCalculating() {
    return this._getParagraphs()
      .filter(para => para.length > PARA_MIN_NUMBER_OF_LETTERS)
      .map(para => new TextStater(para).getNumberOfWords());
  }

  findRepeatingWords() {
    const arr = [
      ...this._getAdverbs(),
      ...this._getNouns(),
      ...this._getVerbs(),
      ...this._getAdjectives()
    ].sort();

    var a = [],
      b = [],
      prev;

    arr.sort();
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] !== prev) {
        a.push(arr[i]);
        b.push(1);
      } else {
        b[b.length - 1]++;
      }
      prev = arr[i];
    }

    return [a, b];
  }
}

export default TextStater;

class GraduationEvaluator {
  constructor(topic, keys, paragraphTopics, paragraphKeys, text) {
    this.topic = topic;
    this.keys = keys;
    this.paragraphTopics = paragraphTopics;
    this.paragraphKeys = paragraphKeys;
    this.text = text;
    this.stater = new TextStater(text);
  }

  __test() {
    console.log("\n----------------------------------------------");
    console.log("Téma: ", this.topic);
    console.log("Klíčová slova: ", this.keys);
    console.log("Klíčové body odstavců: ", this.paragraphTopics);
    console.log("Klíčová slova odstavců: ", this.paragraphKeys);
    console.log("----------------------------------------------");
    console.log("Text:\n", this.text);
    console.log("----------------------------------------------");
  }

  __calcTest() {
    console.log("----------------------------------------------");
    console.log("Téma splněno na: ", this.content(), "%");
    console.log("Počet slov: ", this.textLength());
    console.log("Odstavce dodrženy na: ", this.contentRange(), "%");
    console.log("Obsah odstavců dodržen na: ", this.contentDetails());
    console.log("Koheze textu: ", this.cohesion(), "%");
    console.log("Organizace textu: ", this.organisation());
    console.log("Struktura dodržena: ", this.structure());
    console.log("Počet často opakujících se slov: ", this.textRepetition());
    console.log("Unikátnost slov: ", this.vocabulary(), "%");
    console.log(this.textContinuityMeansRange());
  }

  content() {
    const res =
      (((this.stater.topicEvaluating(this.keys, this.topic).length * 100) /
        this.stater.getNumberOfWords()) *
        100) /
      THEME_LIMIT;

    return res > 100 ? 100 : res;
  }

  textLength() {
    return this.stater.getNumberOfWords();
  }

  contentRange() {
    const para = this.stater
      ._getParagraphs()
      .filter(para => para.length > PARA_MIN_NUMBER_OF_LETTERS).length;

    const points = this.paragraphTopics.length;

    if (points > para) {
      return (para * 100) / points;
    } else if (para > points) {
      return (points * 100) / para;
    } else return 100;
  }

  contentDetails() {
    const paraNum = this.stater
      .getNumberOfWordsInParagraphs()
      .filter(para => para > 8);

    const paraTopics = this.stater.paragraphTopicEvaluating(
      this.paragraphKeys,
      this.paragraphTopics
    );

    const topicResult = paraTopics.map(
      topic => (topic.length * 100) / paraNum[paraTopics.indexOf(topic)]
    );

    return topicResult.map(res =>
      (res * 100) / PARA_THEME_LIMIT > 100
        ? 100
        : (res * 100) / PARA_THEME_LIMIT
    );
  }

  textContinuityMeansRange() {
    const cons = this.stater.getConjunctionsRepeating();
    const len = cons[0].length;
    const sum = cons[1].reduce((p, c) => p + c, 0);
    const res = Math.ceil(sum / 5) - len;
    return res <= 0 ? 0 : res;
  }

  organisation() {
    const para = this.stater._getParagraphs();

    const firstName = this.stater._getNamesInText(para[0]).length;
    const first = this.stater._getAbbreviationsInText(para[0]).length;
    const last = this.stater._getNamesInText(para[para.length - 1]).length;

    return [
      (first > 0 || firstName > 0) && para[0].length < 40,
      last > 0 && para[para.length - 1].length < 40
    ];
  }

  cohesion() {
    const times = this.stater.getTextTimes();
    let last = times[0];
    let count = 0;
    times.forEach(time => {
      if (time != last) {
        ++count;
        last = time;
      }
    });
    return (count * 100) / this.stater._getSentences().length;
  }

  structure() {
    const num = this.stater.paragraphDifferenceCalculating();
    const dif = (MAX_NUMBER_OF_WORDS - 10) / num.length;
    const coef = dif / 2;
    const res = num.map(met => dif - coef < met && dif + coef > met);

    return res;
  }

  textRepetition() {
    const rep = this.stater.findRepeatingWords();
    let filt = [];

    for (let i in rep[1]) {
      if (rep[1][i] > MOST_REPEATED_WORDS) {
        for (let j = 0; j < Math.floor(rep[1][i] / MOST_REPEATED_WORDS); j++) {
          filt.push(rep[1][i]);
        }
      }
    }

    console.log(filt);
    return filt.length;
  }

  vocabulary() {
    const rep = this.stater.findRepeatingWords()[1];
    const avg = rep.reduce((p, c) => p + c, 0) / rep.length;

    return 100 / avg > 0 ? 100 / avg : 0;
  }

  grammarMeansRange() {
    let coef = [];
    if (
      this.stater._getHybernated().length &&
      this.stater._getHybernated().length > 0
    )
      coef.push(...this.stater._getHybernated());
    if (
      this.stater._getClauses().length &&
      this.stater._getClauses().length > 0
    )
      coef.push(...this.stater._getClauses());
    if (
      this.stater._getAdverbs().length &&
      this.stater._getAdverbs().length > 0
    )
      coef.push(...this.stater._getAdverbs());
    if (
      this.stater._getConjunctions().length &&
      this.stater._getConjunctions().length > 0
    )
      coef.push(...this.stater._getConjunctions());
    if (this.stater._getTheme().length && this.stater._getTheme().length > 0)
      coef.push(...this.stater._getTheme());

    return coef.map(co =>
      co[co.length - 1] == " "
        ? co.slice(0, co.length - 1).toLowerCase()
        : co.toLowerCase()
    );
  }
}

// const topic =
//   "Unformal letter for friend Lucy about reccently bought house in USA";

// const topicKeys = ["letter", "move", "house", "visit", "new", "past"];

/*
const topic = "Two nasa pilots discovered new planet";
const topicKeys = ["space", "nasa", "pilots", "planets", "ship"];
*/

// const paragraphTopics = [
//   "Tell about reason, why are you writing a letter.",
//   "Describe new house a lot and compare him with your recent house.",
//   "Tell about good thinks and possitives of new house, also mention negatives and bad things and describe why.",
//   "Invite Lucy for visit your house."
// ];

// const paragraphKeys = [
//   ["reason", "writing", "letter", "why", "explain"],
//   ["describe", "new", "house", "past", "compare"],
//   ["possitives", "negatives", "house", "describe", "why"],
//   ["invite", "invitation", "visit", "home", "future"]
// ];

// const text =
//   "Hi Lucy,\n\nI‘m writing to you because I have just moved to a new flat in the city. I‘d like to tell you some news about it because it‘s just great in here.\n\nOur new flat‘s very large and fully furnished. It’s also equipped with appliances. There are five spacious rooms, a modern kitchen and a balcony with a beautiful view of the city. The place is much larger than the flat we lived in before. You’d love it.\n\nI like my new neighbours. When we arrived, they came right away and introduced themselves. There‘s only one thing I don’t like. It‘s the distance from here to my school because I have to wake up early.\n\nI think you must come to my new place and see all of it. Do you have time this Friday?\n\n See you,\n\n Caroline";

/*
const text =
  "Hello Mary,\n\nHow are you? I am super. I am writing because I want to describe you my house. there live with a family and is very big, a beautiful, a pleasing. At home is ten rooms which have small, big, yellow, white, red, green and blue. Our house have green colour, white windows, brown door and red a roof.\n\n Our new house is about a lot beautiful than our old house. At new house is life nice beacuse is tidy and there are all news and also have beautiful garden and a swimming pool. And red roof and brown door and shote windows are nice also.\n\n I am invite you also to my home because I describe you my house.\n\n See you soon,\n\nTom";

*/
// const eva = new GraduationEvaluator(
//   topic,
//   topicKeys,
//   paragraphTopics,
//   paragraphKeys,
//   text
// );

// eva.__test();
// eva.__calcTest();

class ResultCalculator {
  constructor(evalu) {
    this.evalu = evalu;
  }

  getResult(cb) {
    axios
      .post("http://127.0.0.1:8004/api", {
        text: this.evalu.text
      })
      .then(res => {
        cb({
          id: [
            "I – Zpracování",
            "II – Organizace",
            "III – Slovní zásoba",
            "IV – Mluvnické prostředky"
          ],
          A: [
            this.part1a(),
            this.part2a(),
            this.part3a(res.data),
            this.part4a(res.data)
          ],
          B: [
            this.part1b(),
            this.part2b(),
            this.part3b(),
            this.part4b(res.data)
          ]
        });
      });
  }

  countPartResult(arr) {
    const avg = arr.reduce((p, c) => p + c, 0) / arr.length;
    if (arr[0] > avg) return Math.ceil(avg);
    else return Math.floor(avg);
  }

  countMarkFromPercentage(num) {
    if (num < 48) return 0;
    else if (num < 64) return 1;
    else if (num < 80) return 2;
    return 3;
  }

  countMarkFromDownPercentage(num) {
    if (num > 59) return 0;
    else if (num > 46) return 1;
    else if (num > 33) return 2;
    return 3;
  }

  countMarkFromNumberOfItems(num) {
    if (num <= 3) return 3;
    else if (num <= 6) return 2;
    else if (num <= 9) return 1;
    else return 0;
  }

  countMarkFromMiddlePercentage(num) {
    if (num > 68) return 3;
    else if (num > 56) return 2;
    else if (num > 44) return 1;
    return 0;
  }

  part1a() {
    let resultA = 3;
    let resultB = 3;
    let resultC = 3;

    for (let i in this.evalu.organisation())
      if (!this.evalu.organisation()[i]) resultB--;

    for (let i in this.evalu.structure())
      if (!this.evalu.structure()[i]) resultA--;

    let wordCount = this.evalu.textLength;
    if (
      (wordCount > 150 && wordCount <= 180) ||
      (wordCount < 120 && wordCount >= 90)
    )
      resultC - 1;
    else if (
      (wordCount > 180 && wordCount <= 210) ||
      (wordCount < 90 && wordCount >= 60)
    )
      resultC - 2;
    else if (wordCount > 210 || wordCount < 60) resultC - 3;

    return resultA == 0
      ? 0
      : this.countPartResult([resultA, resultB == 1 ? 0 : resultB, resultC]);
  }

  part1b() {
    let resultA = 3;
    let resultB = 3;
    let resultC = 3;

    for (let i in this.evalu.structure())
      if (!this.evalu.structure()[i]) resultA--;

    resultB = this.countMarkFromPercentage(this.evalu.content());

    resultA = this.countPartResult(
      this.evalu.contentDetails().map(per => this.countMarkFromPercentage(per))
    );

    resultC = this.countMarkFromPercentage(this.evalu.content());

    return resultA == 0 ? 0 : this.countPartResult([resultA, resultB, resultC]);
  }

  part2a() {
    let resultA = 3;
    let resultB = 3;

    resultA = this.countMarkFromDownPercentage(this.evalu.cohesion());

    resultB = this.countPartResult(
      this.evalu.contentDetails().map(per => this.countMarkFromPercentage(per))
    );

    return this.countPartResult([resultA, resultB]);
  }

  part2b() {
    let resultB = 3;
    let resultC = 3;

    resultC = this.countMarkFromNumberOfItems(this.evalu.textRepetition());

    resultB -= this.evalu.textContinuityMeansRange();

    return this.countPartResult([resultB <= 0 ? 0 : resultB, resultC]);
  }

  part3a(mistakes) {
    let grammar = [];
    let style = [];

    mistakes.forEach(row => {
      if (row.rule == "MORFOLOGIK_RULE_EN_GB") grammar.push(row);
      else style.push(row);
    });

    let resultA = 3;
    let resultB = 3;
    let resultC = 3;

    resultA -= Math.floor(grammar.length / 3);
    resultB -= Math.floor(style.length / 3);

    let wordCount = this.evalu.textLength;
    if (wordCount < 120 && wordCount >= 90) resultC - 1;
    else if (wordCount < 90 && wordCount >= 60) resultC - 2;
    else if (wordCount < 60) resultC - 3;

    let re = [resultA, resultB];
    if (resultC != 3) re.push(resultC);

    return this.countPartResult(re);
  }

  part3b() {
    return this.countMarkFromMiddlePercentage(this.evalu.vocabulary());
  }

  part4a(mistakes) {
    let gram = mistakes
      .map(row => {
        if (row.rule == "MORFOLOGIK_RULE_EN_GB") return row;
        else return {};
      })
      .filter(row => row.position);

    let style = mistakes
      .map(row => {
        if (row.rule != "MORFOLOGIK_RULE_EN_GB") return row;
        else return {};
      })
      .filter(row => row.position);

    let resultA = 3;
    let resultB = 3;
    let resultC = 3;

    resultA -= Math.floor(style.length / 5);
    resultB -= Math.floor(gram.length / 3);

    let wordCount = this.evalu.textLength;
    if (wordCount < 120 && wordCount >= 90) resultC - 1;
    else if (wordCount < 90 && wordCount >= 60) resultC - 2;
    else if (wordCount < 60) resultC - 3;

    return this.countPartResult([resultA, resultB, resultC]);
  }

  part4b(mistakes) {
    const coef = this.evalu.grammarMeansRange();

    const forma = coef.filter((value, index, array) => {
      return array.indexOf(value) == index && value != "";
    });
    const res = (100 * (forma.length - mistakes.length)) / coef.length;

    return this.countMarkFromMiddlePercentage(res);
  }
}

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const http = require("http");

const config = {
  name: "text-statistics-service",
  port: "8005"
};

const app = express();
const server = http.createServer(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
  cors({
    origin: ["*"],
    methods: ["GET", "POST"]
  })
);

app.get("/", (req, res, next) => {
  res.send(`Running ${config.name} on port: ${config.port}.`);
});

app.post("/api", (req, res, next) => {
  if (
    req.body.topic &&
    req.body.topicKeys &&
    req.body.paragraphTopics &&
    req.body.paragraphKeys &&
    req.body.text
  ) {
    const calc = new ResultCalculator(
      new GraduationEvaluator(
        req.body.topic,
        req.body.topicKeys,
        req.body.paragraphTopics,
        req.body.paragraphKeys,
        req.body.text
      )
    );

    calc.getResult(result => {
      console.log(result);
      res.send(result);
    });
  } else res.send({ status: false, error: "Missing all data" });
});

app.set("port", config.port);

server.listen(config.port);

server.on("listening", () => {
  let addr = server.address();
  let bind = typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
  console.log(`Listening on ${bind}`);
});
